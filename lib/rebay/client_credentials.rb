
# frozen_string_literal: true

module Rebay
  # https://developer.ebay.com/api-docs/static/oauth-client-credentials-grant.html
  class ClientCredentials
    OAUTH_ENDPOINT = 'https://api.ebay.com/identity/v1/oauth2/token'

    def initialize(app_id: Rebay::Api.app_id, cert_id: Rebay::Api.cert_id)
      @app_id = app_id
      @cert_id = cert_id
    end

    def retrieve_token
      JSON.parse(request.body)
    end

    private

    attr_reader :app_id, :cert_id

    def request
      req = Net::HTTP::Post.new(url.path)
      req.basic_auth(app_id, cert_id)
      req.set_form_data(payload)
      Net::HTTP.start(url.host, 443, use_ssl: true) { |http| http.request(req) }
    end


    def payload
      { grant_type: 'client_credentials',
        scope: 'https://api.ebay.com/oauth/api_scope' }
    end

    def url
      URI.parse(OAUTH_ENDPOINT)
    end
  end
end
