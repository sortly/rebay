require 'net/http'
require 'json'
require 'uri'

module Rebay
  class InvalidAccessTokenError < RuntimeError; end

  class Api
    # default site is EBAY_US, for other available sites see eBay documentation:
    # http://developer.ebay.com/DevZone/merchandising/docs/Concepts/SiteIDToGlobalID.html

    EBAY_US = 0

    class << self
      attr_accessor :app_id, :default_site_id, :sandbox, :cert_id

      def base_url
        [base_url_prefix,
         sandbox ? "sandbox" : nil,
         base_url_suffix].compact.join('.')
      end

      def base_url_prefix
        "https://svcs"
      end

      def base_url_suffix
        "ebay.com"
      end

      def sandbox
        @sandbox ||= false
      end

      def default_site_id
        @default_site_id || EBAY_US
      end

      def configure
        yield self if block_given?
      end
    end

    protected

    attr_accessor :auth_token

    def get_json_response(url)
      uri = URI.parse(url)
      https = Net::HTTP.new(uri.host, uri.scheme == 'https' ? 443 : 80)
      if uri.scheme == 'https'
        https.ssl_version = :TLSv1_2
        https.use_ssl = true
      end

      response =
        https.start do |https|
          Rebay::Response.new(JSON.parse(https.request_get(uri, headers).body))
        end

      if response.failure? && response.response.dig('Errors', 'ErrorCode') == "1.32"
        raise InvalidAccessTokenError, response.response.inspect
      end

      response
    end

    def build_rest_payload(params)
      payload = ''
      unless params.nil?
        params.keys.each do |key|
          payload += URI::DEFAULT_PARSER.escape "&#{key}=#{params[key]}"
        end
      end
      return payload
    end

    def headers
      return {} unless auth_token.present?

      { 'X-EBAY-API-IAF-TOKEN' => auth_token }
    end
  end
end
